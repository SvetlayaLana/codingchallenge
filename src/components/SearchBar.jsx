import React from 'react';
import './SearchBar.css';
import {getYears} from '../utils/helper';

let years = getYears();

function SearchBar({ searchValue, searchYear, onSearchChange, onYearChange, handleSubmit }) {

    return (
        <form className="search-bar" onSubmit={handleSubmit}>
            <input className="search-bar__title" type="search" value={searchValue} onChange={onSearchChange} />
            <select className="search-bar__year" value={searchYear} onChange={onYearChange}>
                <option value="">None</option>
                {years.map(year => <option value={year} key={year}>{year}</option>)}
            </select>
            <button type='submit'>Search</button>
        </form>
    );
}

export default SearchBar;
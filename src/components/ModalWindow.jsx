import React, { useEffect, useState } from 'react';
import ModalContent from './ModalContent';
import './ModalWindow.css';

function ModalWindow({ imdbID, onCloseModal }) {

    const [cardDetails, setCardDetails] = useState({});

    console.log(imdbID);

    useEffect(() => {
        fetch(`http://www.omdbapi.com/?apikey=4b601aab&i=${imdbID}`)
            .then(response => response.json())
            .then(respJson => {
                setCardDetails(respJson);
            });
    }, []);

    return (
        <>
            <div onClick={onCloseModal} className="modal-window"></div>
            <ModalContent cardDetails={cardDetails} onCloseModal={onCloseModal}/>
        </>
    );
}

export default ModalWindow;
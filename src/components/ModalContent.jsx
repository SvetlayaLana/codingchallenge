import React from 'react';
import './ModalContent.css';

function ModalContent({cardDetails, onCloseModal}){
    const {Title, Director, Actors, Released, BoxOffice, Country, Genre, Runtime, Plot, imdbRating, imdbVotes, Poster} = cardDetails;
    return(
        <div className="modal-window__content">
            <button className="modal-window__button" onClick={onCloseModal}>X</button>
            <div>Title: {Title}</div>
            <div>Director: {Director}</div>
            <div>Actors: {Actors}</div>
            <div>Released: {Released}</div>
            <div>Box office: {BoxOffice}</div>
            <div>Country: {Country}</div>
            <div>Genre: {Genre}</div>
            <div>Duration: {Runtime}</div>
            <div>Plot: {Plot}</div>
            <div>Rating: {imdbRating}/{imdbVotes}</div>
            <a download='poster.jpg' href={Poster} title={Title}>
                <img src={Poster} alt="poster" />
            </a>
        </div>
    )
}

export default ModalContent;
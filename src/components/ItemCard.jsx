import React from 'react';
import './ItemCard.css';

function ItemCard({item, onShowModal}){
    const { Title , Year , Poster , imdbID } = item;
    const emptyImage = './images/noimage.jpg';

    const handleShowModal = () => {
        onShowModal(imdbID);
    }

    return(
        <div className="item-card" onClick={handleShowModal}>
            <img className="item-card__poster" src={Poster === 'N/A' ? emptyImage : Poster} alt="poster" />
            <div className="item-card__title">{Title}</div>
            <div className="item-card__year">{Year}</div>
        </div>
    )
}

export default ItemCard;
import React from 'react';
import ItemCard from './ItemCard';
import './ItemsContainer.css';

function ItemsContainer({cardItems, onShowModal, error}){
    return(
        <div className="items">
            {!error && cardItems ? cardItems.map(item => <ItemCard key={item.imdbID} item={item} onShowModal={onShowModal}/>) : error}
        </div>
    )
}

export default ItemsContainer;
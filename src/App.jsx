import React, { useState, useEffect } from 'react';
import './App.css';
import ItemsContainer from './components/ItemsContainer';
import SearchBar from './components/SearchBar';
import ModalWindow from './components/ModalWindow';
import {getFromPage} from './utils/helper';

function App() {

    const [cardItems, setCardItems] = useState(null);
    const [searchValue, setSearchValue] = useState('');
    const [searchYear, setSerchYear] = useState('');
    const [showModal, setShowModal] = useState(false);
    const [currentCardID, setCurrentCardID] = useState('');

    useEffect(() => {
        fetch("http://www.omdbapi.com/?apikey=4b601aab&s=*react*&type=movie&page=2")
            .then(response => response.json())
            .then(respJson => {
                setCardItems(respJson.Search);
            });

    }, []);

    const onSearchChange = (e) => {
        setSearchValue(e.target.value);
    };

    const onYearChange = (e) => {
        setSerchYear(e.target.value);
    };

    const onShowModal = id => {
        setShowModal(true);
        setCurrentCardID(id);
    };

    const onCloseModal = () => {
        setShowModal(false);
        setCurrentCardID('');
    };

    const handleSubmit = e => {
        e.preventDefault();
        fetch(`http://www.omdbapi.com/?apikey=4b601aab&&s=*react*&type=movie&t=${searchValue}&y=${searchYear}`)
            .then(response => response.json())
            .then(respJson => {
                console.log(respJson);
                setCardItems(respJson.Search);
            });
    }

    const handlePage = e =>{
        let id = e.target.id;
        fetch(`http://www.omdbapi.com/?apikey=4b601aab&s=*react*&type=movie&page=${id}`)
            .then(response => response.json())
            .then(respJson => {
                setCardItems(respJson.Search);
            });
    };

    return (
        <>
            <SearchBar searchValue={searchValue} searchYear={searchYear} onSearchChange={onSearchChange} onYearChange={onYearChange} handleSubmit={handleSubmit} />
            <ItemsContainer cardItems={cardItems} onShowModal={onShowModal} />
            {showModal && <ModalWindow imdbID={currentCardID} onCloseModal={onCloseModal} />}
            <button id='1' onClick={handlePage}>Page 1</button>
            <button id='2' onClick={handlePage}>Page 2</button>
        </>
    );
}

export default App;
